<?php
/**
 * Local variables
 * @var \Phalcon\Mvc\Micro $app
 */

use Datto\JsonRpc\Http\Client;

/**
 * Add your routes here
 */
$app->get('/', function () {
//     TODO add CSRF
    echo $this['view']->render('index');
});

$app->post('/', function() {
    $client = new Client('http://users');
    $login = $this->request->getPost('login');
    $password = $this->request->getPost('password');


    if ($login && $password) {
        $client->query('login', [
            'login' => $login,
            'password' => $password,
        ], $result);

        try {
            $client->send();
            $this->view->result = $result;
        } catch (ErrorException $exception) {
            $this->view->result =  "Server error";
        }
    } else {
        $this->view->result = 'Invalid params';
    }
    echo $this['view']->render('login');
});

/**
 * Not found handler
 */
$app->notFound(function () use($app) {
    $app->response->setStatusCode(404, "Not Found")->sendHeaders();
    echo $app['view']->render('404');
});
