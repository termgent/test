<?php

use Datto\JsonRpc\Evaluator;
use Datto\JsonRpc\Exceptions\ArgumentException;
use Datto\JsonRpc\Exceptions\MethodException;

class Api implements Evaluator
{
    public function evaluate($method, $arguments)
    {
        $classMethods = get_class_methods('Api');
        if(in_array($method, $classMethods)) {
            return self::$method($arguments);
        } else {
            throw new MethodException();
        }
    }

    private function login($arguments)
    {
        if (!(array_key_exists('login', $arguments) && array_key_exists('password', $arguments))) {
            throw new ArgumentException();
        }
        $user = User::query()
            ->where('login = :login:')
            ->bind(['login' => $arguments['login']])
            ->execute()->getFirst();
        if($user && $user->checkPassword($arguments['password'])) {
            return 'Successful authorization.';
        }
        return 'Wrong login or password.';
    }
}
