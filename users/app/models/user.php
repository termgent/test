<?php

use Phalcon\Mvc\Model;
use Phalcon\Validation;

class User extends Model
{
    /**
     *
     * @var integer
     */
    protected $id;
    /**
     *
     * @var string
     */
    protected $login;
    /**
     *
     * @var string
     */
    protected $password;

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }
    /**
     * Method to set the value of field name
     *
     * @param string $login
     * @return $this
     */
    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }
    /**
     * Method to set the value of field password
     *
     * @param string $password
     * @return $this
     */
    public function setPassword($password)
    {
        // TODO add encrypt password
        $this->password = $password;
        return $this;
    }
    public function getId()
    {
        return $this->id;
    }
    /**
     * Returns the value of field login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }
    /**
     * Returns the value of field password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getSource()
    {
        return 'users';
    }

    public function checkPassword($s) {
        return $this->password === $s;
    }
}