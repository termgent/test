<?php

/**
 * Registering an autoloader
 */
$loader = new \Phalcon\Loader();

$loader->registerFiles([ BASE_PATH . "/vendor/autoload.php" ]);

$loader->registerDirs(
    [
        $config->application->modelsDir,
    ]
)->register();
