<?php

use Datto\JsonRpc\Server;

$app->post('/', function () use ($app) {
    $message = $app->request->getRawBody();
    $api = new Api();
    $server = new Server($api);
    $reply = $server->reply($message);
    return $reply;
});